#!/usr/bin/env python
"""
    apk_analyzer performs various tasks for analyzing apks

    Copyright (C) 2018 Zach Yannes
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>
"""
import os, sys
import re
# from datetime import datetime
import argparse

from os import listdir
from os.path import isfile, join
import csv

from collections import OrderedDict
import gzip
import treelib
from treelib import Node, Tree
import errno
from mycommand import Command
from mythreadcommand import ThreadedCommand
import tree_utils
from tree_utils import *
from tqdm import tqdm
import logging
logging.basicConfig(format = '%(message)s', level=logging.INFO)
logger = logging.getLogger(__name__)
# logger.setLevel(logging.INFO)

APKANALYZER = '{}/tools.26.1.1/bin/apkanalyzer'.format(os.environ['ANDROID_SDK'])
if not os.path.isfile(APKANALYZER):
    APKANALYZER = '{}/tools/bin/apkanalyzer'.format(os.environ['ANDROID_SDK'])

#
PACKAGE_PATTERN = re.compile('P\s*(?P<state>[xkrd])\s*(?P<defined>[\d]+)\s*(?P<referenced>[\d]+)\s*(?P<size>[\d]+)\s*(?P<name>[^\s]+)\s*(?P<extra>.*)')
CLASS_PATTERN = re.compile('C\s*(?P<state>[xkrd])\s*(?P<defined>[\d]+)\s*(?P<referenced>[\d]+)\s*(?P<size>[\d]+)\s*(?P<name>[^\s]+)')
# METHOD_PATTERN = re.compile('M\s*(?P<state>[xkrd])\s*(?P<defined>[\d]+)\s*(?P<referenced>[\d]+)\s*(?P<size>[\d]+)\s*(?P<name>[^\s]+)\s*(?P<method>.*)')
METHOD_PATTERN = re.compile('M\s*(?P<state>[xkrd])\s*(?P<defined>[\d]+)\s*(?P<referenced>[\d]+)\s*(?P<size>[\d]+)\s*(?P<name>[^\s]+)\s*(?P<methodname>[^\(]+)\((?P<params>[^)]*)\)')
FIELD_PATTERN = re.compile('F\s*(?P<state>[xkrd])\s*(?P<defined>[\d]+)\s*(?P<referenced>[\d]+)\s*(?P<size>[\d]+)\s*(?P<name>[^\s]+)\s*(?P<data_type>[^\s]+)\s*(?P<field>.*)')

APK_PATTERN = re.compile('(?P<appname>[\w\d\_]+)_(?P<version>[\d\.]+)(\-(?P<extra>).+)?_APKTrunk')

def parseArgs():
    parser = argparse.ArgumentParser(
        prog='PROG',
        description='Analyze Android apk file',
    )

    parser.add_argument('input',
                        help = 'Input (apk or directory)')

    parser.add_argument('-o', '--outdir',
                        help = 'Directory to output apk data into',
                        default = '.')

    parser.add_argument('-d', '--dryrun',
                        help = 'Do not save trees to file',
                        action = 'store_true')

    parser.add_argument('-v', '--verbose',
                        help = 'Print debug information',
                        action = 'store_true')

    parser.add_argument('-f', '--force',
                        help = 'Force overwriting of files',
                        action = 'store_true')

    parser.add_argument('-c', '--compare',
                        help = 'Compare app tree directories',
                        choices = ['classes', 'fields', 'methods', 'packages'],
                        nargs = '+',
                        default = ['classes', 'packages'])

    parser.add_argument('--table',
                        help = 'Generate table from data',
                        action = 'store_true')

    args, unknown = parser.parse_known_args()
    args = vars(args)

    if args['verbose']:
        logger.setLevel(logging.DEBUG)
        logging.getLogger('tree_utils').setLevel(logging.DEBUG)
    else:
        logger.setLevel(logging.INFO)
        logging.getLogger('tree_utils').setLevel(logging.INFO)

    logger.info('Args: {}'.format(args))

    return args

def list_directories(topdir, prefix = None):
    return [os.path.join(topdir, name) for name in os.listdir(topdir) if os.path.isdir(os.path.join(topdir, name))]

def list_files(topdir, prefix = None, ext = '.java'):
    file_list = []
    for dirpath, dirnames, files in os.walk(topdir):
        file_list += [os.path.join(dirpath, f) for f in files if f.endswith(ext) and (prefix and f.startswith(prefix))]
    return file_list

def listFiles(directory, prefix = None, ext = '.java'):
    if prefix:
        files = []
        for f in listdir(directory):
            logging.info('searching prefix {} == {}'.format(prefix, f))
            files.append(f)
            # files = sorted([join(directory, f) for f in listdir(directory) if f.startswith(prefix) and f.endswith(ext)])
    else:
        files = sorted([join(directory, f) for f in listdir(directory) if join(directory, f).endswith(ext)])
    return files

def parseLine(pattern, line):
    m = re.match(pattern, line)
    if not m:
        return None
    return m.groupdict()

APK_SUMMARY = 0
APK_FILESIZE = 1
APK_DOWNLOADSIZE = 2
APK_FEATURES = 3
APK_VERSIONNAME = 4
APK_MINSDK = 5
APK_TARGETSDK = 6
APK_PERMISSIONS = 7
APK_APPLICATIONID = 8
APK_DEXPACKAGES = 9
APK_DEXCODE = 10
APK_DEXCODEMETHOD = 11

COMMAND_FORMATS = [
    '{apkanalyzer} apk summary {apk}',
    '{apkanalyzer} apk file-size {apk}',
    '{apkanalyzer} apk download-size {apk}',
    '{apkanalyzer} apk features {apk}',
    '{apkanalyzer} manifest version-name {apk}',
    '{apkanalyzer} manifest min-sdk {apk}',
    '{apkanalyzer} manifest target-sdk {apk}',
    '{apkanalyzer} manifest permissions {apk}',
    '{apkanalyzer} manifest application-id {apk}',
    '{apkanalyzer} dex packages {apk}',
    '{apkanalyzer} dex code {apk} --class {classname}'
    '{apkanalyzer} dex code {apk} --class {classname} --method {methodname}',
]

def apk_command(cmd_id, kwargs):
    fmt = COMMAND_FORMATS[cmd_id]
    cmdstr = fmt.format(apkanalyzer = APKANALYZER, **kwargs)
    cmd = ThreadedCommand(cmdstr)
    return cmd

def apk_summary(apk):
    cmd = '{} apk summary {}'.format(APKANALYZER, apk)
    proc = Command(cmd)
    log = proc.run()
    return log[0].split()

def apk_file_size(apk):
    cmd = '{} apk file-size {}'.format(APKANALYZER, apk)
    proc = Command(cmd)
    log = proc.run()
    return log[0]

def apk_download_size(apk):
    cmd = '{} apk download-size {}'.format(APKANALYZER, apk)
    proc = Command(cmd)
    log = proc.run()
    return log[0]

def apk_features(apk):
    cmd = '{} apk features {}'.format(APKANALYZER, apk)
    proc = Command(cmd)
    log = proc.run()
    return log

def manifest_version_name(apk):
    cmd = '{} manifest version-name {}'.format(APKANALYZER, apk)
    proc = Command(cmd)
    log = proc.run()
    return log[0]

def manifest_min_sdk(apk):
    cmd = '{} manifest min-sdk {}'.format(APKANALYZER, apk)
    proc = Command(cmd)
    log = proc.run()
    return log[0]

def manifest_target_sdk(apk):
    cmd = '{} manifest target-sdk {}'.format(APKANALYZER, apk)
    proc = Command(cmd)
    log = proc.run()
    return log[0]

def manifest_permissions(apk):
    cmd = '{} manifest permissions {}'.format(APKANALYZER, apk)
    proc = Command(cmd)
    log = proc.run()
    return log

def manifest_application_id(apk):
    cmd = '{} manifest application-id {}'.format(APKANALYZER, apk)
    proc = Command(cmd)
    log = proc.run()
    return log[0]

def dex_packages(apk):
    cmd = '{} dex packages {}'.format(APKANALYZER, apk)
    proc = Command(cmd)
    log = proc.run()
    return log

def dex_code(apk, classname, methodname = None):
    cmd = '{} dex code {} --class {}'.format(APKANALYZER, apk, classname)
    if methodname:
        cmd = '{} --method {}'.format(cmd, methodname)
    proc = Command(cmd)

    log = proc.run()
    return log

def fix_dex_package(item):
    return {
        'name': item['name'],
        'defined': int(item['defined']),
        'referenced': int(item['referenced']),
        'size': int(item['size']),
        'state': str(item['state']),
    }

def fix_dex_class(item):
    return {
        'name': item['name'],
        'defined': int(item['defined']),
        'referenced': int(item['referenced']),
        'size': int(item['size']),
        'state': str(item['state']),
    }

def fix_dex_method(item):
    return {
        'name': item['name'],
        'defined': int(item['defined']),
        'referenced': int(item['referenced']),
        'methodname': str(item['methodname']),
        'params': str(item['params']),
        'size': int(item['size']),
        'state': str(item['state']),
    }

def fix_dex_field(item):
    return {
        'name': item['name'],
        'defined': int(item['defined']),
        'referenced': int(item['referenced']),
        'field': str(item['field']),
        'data_type': str(item['data_type']),
        'size': int(item['size']),
        'state': str(item['state']),
    }

def parse_dex_packages(lines):
    packages = []
    methods = []
    fields = []
    classes = []
    for line in tqdm(lines, leave = False):
        tokens = line.split()

        item = parseLine(PACKAGE_PATTERN, line)
        if item:
            # packages.append(fix_dex_package(item))
            packages.append(item)
            continue
        item = parseLine(CLASS_PATTERN, line)
        if item:
            # classes.append(fix_dex_class(item))
            classes.append(item)
            continue
        item = parseLine(METHOD_PATTERN, line)
        if item:
            # methods.append(fix_dex_method(item))
            methods.append(item)
            continue
        item = parseLine(FIELD_PATTERN, line)
        if item:
            # fields.append(fix_dex_field(item))
            fields.append(item)
            continue
    return classes, fields, methods, packages

def parseApkData(apk, compare = ['classes', 'packages', 'fields', 'methods']):
    log = dex_packages(apk)
    i = 0
    packages = []
    methods = []
    fields = []
    classes = []
    classes, fields, methods, packages = parse_dex_packages(log)
    logger.debug('{} classes'.format(len(classes)))
    logger.debug('{} fields'.format(len(fields)))
    logger.debug('{} methods'.format(len(methods)))
    logger.debug('{} packages'.format(len(packages)))

    if 'classes' not in compare:
        classes = []
    if 'fields' not in compare:
        fields = []
    if 'methods' not in compare:
        methods = []
    if 'packages' not in compare:
        packages = []

    return classes, fields, methods, packages

def match_state(items, state):
    return [item for item in items if item.get('state') == state]

def readApplicationData(inputdir, choice = None):
    trees = []
    files = list_files(inputdir, prefix = choice, ext = '.dat')
    for f in tqdm(files, leave = False):
        logger.debug('Reading tree from {}'.format(f))
        tree = readTreeFromFile(f)
        logger.debug('Read tree from {}'.format(f))
        trees.append(tree)
    return trees

def extract_name(text, choice):
    fname = '.'.join(text.split('/')[-1].split('.')[:-1])
    name = fname.replace(choice + '_', '')
    return name

def make_filename(choice, name):
    return '{}_{}.dat'.format(choice, name)

def listChoiceTypes(inputdir, choice):
    files = list_files(inputdir, prefix = choice, ext = '.dat')

    names = sorted(list(set([extract_name(f, choice) for f in files])))
    logging.debug('{} {}:{}'.format(len(names), choice, names))
    return names

def listChoiceFiles(inputdir, choice):
    names = listChoiceTypes(inputdir, choice)
    files = []
    all_files = {}
    for name in names:
        all_files[name] = list_files(inputdir, prefix = '{}_{}'.format(choice, name), ext = '.dat')

    logger.debug('All {} files ({}):'.format(choice, len(all_files)))
    for name in all_files:
        logger.debug('\t{} ({}): {}'.format(name, len(all_files[name]), ', '.join(all_files[name])))
    return all_files

def readAllFromDirectory(inputdir, choice = None, dryrun = False):
    trees = []
    all_files = listChoiceFiles(inputdir, choice)

    MAXITER = 4
    apk_trees = {}

    if args['dryrun']:
        sys.exit(0)

    names = [name for name in all_files]
    for name in tqdm(names):
        # apk_trees[name] = readApplicationData(all_files[name], choice = choice)
        apk_trees[name] = []
        for f in tqdm(all_files[name], leave = False):
            apk_trees[name].append(readTreeFromFile(f))
        # apk_trees[name] = [readTreeFromFile(f) for f in all_files[name]]

    for apk in apk_trees:
        logger.debug(apk)
        for tree in apk_trees[apk]:
            logger.debug('\t{}'.format(tree))

    all_trees = [apk_trees[apk] for apk in apk_trees]
    for tree1, tree2 in zip(tqdm(all_trees[:-1]), tqdm(all_trees[1:])):
        union_trees(tree1, tree2)

    return apk_trees


def is_package_parent(data, packagename):
    for key in data:
        if key.startswith(packagename):
            return True
    return False

def generate_table(args):
    classes, fields, methods, packages = dex_packages(args, args['input'])
    packages = sorted(packages, key = lambda x: x['name'].count('.'), reverse = True)

    data = {}
    for p in tqdm(packages):
        name = p.get('name')
        if not is_package_parent(data, name) and ('android.support' in name or name.startswith('edu')):
            logger.debug('Package: {}'.format(p))
            data[name] = p

    data = sorted([data[key] for key in data], key = lambda x: x['name'])
    logger.info('Generating table for {}'.format(args['input']))

    lines = ['\\begin{table}[]']
    lines.append('\\begin{tabular}{|c|c|c|c|c|} \\\\\n\\hline')
    lines.append('\\multicolumn{1}{|c|}{Package} & \\multicolumn{1}{c|}{Defined Methods} & \\multicolumn{1}{c|}{Referenced Methods} & \\multicolumn{1}{c|}{Size (B)} \\\\')

    for d in data:
        line = '{} & {} & {} & {} \\\\'.format(d['name'], d['defined'], d['referenced'], d['size'])
        lines.append(line)

    lines.append('\\end{tabular}\n\\end{table}')
    logger.info('\n'.join(lines))

    '''
    outname = '{}.tex'.format('.'.join(args['input'].split('.')[:-1]))
    with open(outname, 'w') as f:
        f.write('\n'.join(lines))
    '''

if __name__ == '__main__':
    args = parseArgs()

    if args['table']:
        args['compare'] = ['packages']
        generate_table(args)
        sys.exit(0)

    if args['input'].endswith('.apk'):
        baseapk = args['input'].split('/')[-1]
        path = '{}/{}'.format(args['outdir'], '.'.join(baseapk.split('.')[:-1]))
        if os.path.exists(path) and not args['force']:
            logging.info('Already parsed {} into {}. Quitting'.format(args['input'], path))
            sys.exit(0)

        classes, fields, methods, packages = parseApkData(args['input'], args['compare'])
        class_trees, field_trees, method_trees, package_trees = organizeDataIntoTrees(classes, fields, methods, packages)

        if args['verbose']:
            printTrees(class_trees, field_trees, method_trees, package_trees)

        if args['dryrun']:
            sys.exit(0)

        if args['outdir']:
            data = {
                'classes': class_trees,
                'fields': field_trees,
                'methods': method_trees,
                'packages': package_trees,
            }
            logging.info('Saving {} trees'.format(args['input']))
            saveAllTrees(args, data)
    elif args['input'].endswith('.tree.gz'):
        tree = readTreeFromFile(args['input'])
        logger.debug('Tree dir: {}'.format(dir(tree)))
        root = tree.root
        logger.info('Tree root {}: {}'.format(type(root), root))
        for node in tree.all_nodes():
            logger.info('node {}: {}'.format(type(node), node))

        tree.show()
    elif os.path.isdir(args['input']):
        if not args['compare']:
            logger.error('Error: missing -c flag')
            sys.exit(1)
        readAllFromDirectory(args['input'], choice = args['compare'], dryrun = args['dryrun'])
        sys.exit(0)
    else:
        logger.error('Error: invalid input "{}"'.format(args['input']))
