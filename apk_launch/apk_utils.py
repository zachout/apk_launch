#!/usr/bin/env python
"""
    apk_utils contains helper methods for dealing with apks

    Copyright (C) 2018 Zach Yannes
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>
"""
import os, sys
reload(sys)
sys.setdefaultencoding('utf-8')
import subprocess, threading
import signal
import time
import argparse
import re
from mycommand import Command
import xmltodict, json

from tempfile import mkstemp
from shutil import move, copy
from os import remove, close
import fnmatch
import scandir
from gradle_parser import fix_gradle
import logging

from tqdm import tqdm
from concurrent.futures import ProcessPoolExecutor, as_completed

logging.basicConfig(level=logging.INFO, format = '%(message)s')
logger = logging.getLogger(__name__)
# logger.setLevel(logging.INFO)

ERROR_NOTINSTALLED = 3
BUILD_VERSION = '27.0.3' # '26.0.1' # '25.0.3'
GRADLE_VERSION = '4.4.1'
GRADLE_CLASSPATH = '3.0.1'

VERSION_PATTERN = re.compile(r'buildToolsVersion (\'|")(?P<version>[\w\d\.]+)(\'|")')
DEFAULT_ERROR_PATTERN = re.compile('(?P<fail>FAILED)')
ANT_ERROR_PATTERN = re.compile('(?P<fail>BUILD FAILED)')
PATTERN_GRADLE_URL = re.compile('distributionUrl=(?P<url>.+)') # re.compile('distributionUrl=(?P<url>http(s)?\\://services.gradle.org/distributions/gradle)-(?P<version>[\d\.]+-(all|bin)).zip')
PATTERN_GRADLE_CLASSPATH = re.compile('classpath \'(?P<classpath>com\.android\.tools\.build:gradle:.+)\'')

TASK_TIMEOUT = 5 * 60 # 5 minutes

def parseArgs():
    parser = argparse.ArgumentParser(
        prog='PROG',
        description='Build and launch apk using Apache Ant',
    )

    parser.add_argument('rootdir',
                        help = 'Root directory of Android application')

    parser.add_argument('-s', '--stage',
                        help = 'Run stage(s)',
                        choices = ['init', 'build', 'install', 'launch', 'clean', 'info'],
                        default = ['init', 'build', 'install', 'launch'],
                        nargs = '+')

    parser.add_argument('-d', '--dryrun',
                        help = 'Do not execute the stage',
                        action = 'store_true',
                        default = False)

    parser.add_argument('-a', '--activity',
                        help = 'Activity to launch')

    parser.add_argument('-l', '--launch',
                        action = 'store_true',
                        default = False)

    args, unknown = parser.parse_known_args()

    args = vars(args)

    if args.get('launch'):
        args['stage'] = ['launch']

    dirtype = GetProjectType(args['rootdir'])
    if not dirtype:
        sys.exit(1)
    args['dirtype'] = dirtype
    return args

def FindFilesRecursive(rootdir, filename, parent = None):
    found = []
    # logger.debug('Recursively searching {} for {}, {}'.format(rootdir, filename, parent))
    for root, subdir, files in scandir.walk(rootdir):
        files = [f for f in files if not f[0] == '.']
        subdir[:] = [d for d in subdir if not d[0] == '.']
        # logger.debug('Root {}, Subdirectories: {}'.format(root, subdir))
        if parent and not root.endswith(parent):
            continue

        # logger.debug('Root {}, files: {}'.format(root, files))
        if filename in files:
            # return '{}/{}'.format(root, filename)
            # logger.debug('Found {} in {}/{}'.format(filename, root, filename))
            path = os.path.abspath(os.path.join(root, filename))
            found.append(path)

    return sorted(found)

def FindFilePatternRecursive(rootdir, pattern, parent = None):
    found = []
    # logger.debug('Recursively searching {} for {}'.format(rootdir, pattern))
    for root, subdir, files in scandir.walk(rootdir):
        # logger.debug('Root {}, Subdirectories: {}'.format(root, subdir))
        if parent and not root.endswith(parent):
            continue
        for f in files:
            if pattern in f:
            # return '{}/{}'.format(root, filename)
                logger.debug('Found {} in {}/{}'.format(f, root, pattern))
                path = os.path.abspath(os.path.join(root, f))
                found.append(path)

    return sorted(found)

def FindDirectoryRecursive(rootdir, pattern):
    return [os.path.abspath(os.path.join(root, d)) for root, subdir, files in scandir.walk(rootdir) for d in subdir if re.match(pattern, os.path.abspath(os.path.join(root, d)))]
    '''
    found = []
    for root, subdir, files in scandir.walk(rootdir):
        dirs = [os.path.abspath(os.path.join(root, d)) for d in subdir if re.match(pattern, d)]
        found += dirs
    return [d for d in found if d]
    '''

def FindFilePatternFast(rootdir, pattern):
    found = []

    # logger.debug('Recursively searching {} for {}'.format(rootdir, pattern))
    for root, subdir, files in scandir.walk(rootdir):
        # logger.debug('Matching files: {}'.format(files))
        found += [os.path.abspath(os.path.join(root, f)) for f in files if re.match(pattern, f)]
        '''
        for f in fnmatch.filter(files, pattern):
            path = os.path.abspath(os.path.join(root, f))
            found.append(path)
        '''
    return found

def get_parent_directory(path):
    parent = os.path.abspath(os.path.join(path, os.pardir))
    return parent.split('/')[-1]

def FindManifestFileRecursive(rootdir):
    # logger.debug('Searching {} for AndroidManifest.xml'.format(rootdir))
    files = FindFilesRecursive(rootdir, 'AndroidManifest.xml')
    if not files:
        return None
    if len(files) == 1:
        return files[0]

    files = [f for f in files if 'build' not in f.split('/')]
    for f in files:
        if get_parent_directory(f) == 'main':
            return f
    return files[0]

# Can either be: gradle, ant, or maven
def GetProjectType(rootdir):
    files = FindFilesRecursive(rootdir, 'gradlew') # , parent = 'app')
    if files:
        return 'gradle'
    files = FindFilesRecursive(rootdir, 'build.xml')
    if files:
        logger.debug('Got maven project: files {}'.format(files))
        return 'maven'
    files = FindFilesRecursive(rootdir, 'Android.mk')
    if files:
        return 'android'

    logger.error('Error: project {} not setup for gradle, maven, or android'.format(rootdir))
    return 'maven'

def IsGradleProject(rootdir):
    gradlefile = 'gradlew' # 'build.gradle'
    # logger.debug('Checking if {} is gradle project'.format(rootdir))
    files = FindFilesRecursive(rootdir, gradlefile) # , parent = 'app')
    if not files:
        return None

    return [os.path.abspath(os.path.join(f, os.pardir)) for f in files if get_parent_directory(f) != 'gradle']

def findManifestFile(rootdir):
    # return '%s/%s' % (rootdir, 'app/src/main/AndroidManifest.xml')
    return FindManifestFileRecursive(rootdir)

def IsAndroidProject(rootdir):
    filename = findManifestFile(rootdir)
    return True if filename else False

def parseLine(pattern, line):
    m = re.match(pattern, line)
    if m:
        return m.groupdict()
    return None

def checkLauncherActivity(activity):
    if isinstance(activity.get('intent-filter'), dict):
        filters = [activity.get('intent-filter')]
    else:
        filters = activity.get('intent-filter')
    if not filters:
        return None
    for filter in filters:
        name = activity.get('@android:name')
        if not name:
            name = activity.get('@a:name')
        if isinstance(filter, dict):
            actions = [filter.get('action')]
        else:
            actions = filter.get('action')
        category = filter.get('category')
        for action in actions:
            if not action or not category:
                return None

            if isinstance(action, dict):
                actnames = [action.get('@android:name') if action.get('@android:name') else action.get('@a:name')]
            else:
                actnames = [act.get('@android:name') if act.get('@android:name') else act.get('@a:name') for act in action]

            if isinstance(category, dict):
                cats = [category.get('@android:name') if category.get('@android:name') else category.get('@a:name')]
            else:
                cats = [cat.get('@android:name') if cat.get('@android:name') else cat.get('@a:name') for cat in category]

            for actname in actnames:
                for cat in cats:
                    if actname == 'android.intent.action.MAIN' \
                            and cat == 'android.intent.category.LAUNCHER':
                        return name
    return None

def parseManifest(filename, activity = None):
    logger.debug('Parsing Manifest file: {}'.format(filename))
    data = {
        'package': '',
        'activity': '',
        'launcher': False,

    }
    try:
        with open(filename, 'r') as f:
            lines = f.readlines()
    except TypeError as e:
        logger.error('Error on file "{}": {}'.format(filename, e))
        return None

    text = '\n'.join(lines)
    o = xmltodict.parse(text)

    try:
        xmldict = eval(json.dumps(o))
    except NameError as e:
        logger.error('Error with manifest file {}: {}'.format(filename, e))
        return None

    manifest = xmldict.get('manifest')

    data['package'] = manifest.get('@package')
    app = manifest.get('application')
    if not app:
        logger.error('Error: cannot find application in manifest file {}'.format(filename))
        return data

    acts = app.get('activity')
    if not acts:
        logger.error('Error: cannot find launch activity in manifest file {}'.format(filename))
        return data
    data['launcher'] = False
    if isinstance(acts, dict):
        acts = [acts]

    for act in acts:
        for key in act.keys():
            if 'name' == key.split(':')[-1]:
                name = act.get('@android:name')
                if not name:
                    name = act.get('@a:name')
                break
        logger.debug('Checking act {}: "{}"'.format(activity, name))

        if activity and activity in actname:
            name = act.get('@android:name')
            if not name:
                name = act.get('@a:name')
            data['activity'] = name
            break

        if not activity:
            actName = checkLauncherActivity(act)
            if actName:
                data['activity'] = actName
                data['launcher'] = True
                break
        if not data['activity']:
            data['activity'] = name

    return data

def createLaunchCommand(package, activity, launcher = False):
    cmd = 'adb shell am start -n %s/%s' % (package, activity)
    if launcher:
        cmd = 'adb shell monkey -p %s -c android.intent.category.LAUNCHER 1' % (package)
    return cmd

def apkexists(rootdir, findall = False):
    # files = FindFilePatternRecursive(rootdir, '.apk')
    if not findall:
        pattern = re.compile('.*(build/outputs/apk/debug|bin)$')
        dirs = FindDirectoryRecursive(rootdir, pattern) # '(build|bin)')
    else:
        dirs = [rootdir]
    logger.debug('Found {} dirs: {}'.format(len(dirs), dirs))
    # logger.info('Found build directories: {}'.format(dirs))
    files = []
    pattern = re.compile('(.+\.apk)$')
    for d in tqdm(dirs, leave = False):
        files += FindFilePatternFast(d, pattern)

    logger.debug('Found {} apks: {}'.format(len(files), files))
    if not files:
        return None
    # all_paths = [f for f in files if ('build' in f.split('/') or 'bin' in f.split('/')) and not f.endswith('unaligned.apk')]
    return [f for f in files if not f.endswith('unaligned.apk') and os.path.isfile(f)]

def parallel_apkexists(array, n_jobs = 16, front_num = 3):
    if front_num > 0:
        front = [apkexists(a) for a in array[:front_num]]
    if n_jobs==1:
        return front + [apkexists(a) for a in tqdm(array[front_num:])]
    with ProcessPoolExecutor(max_workers = n_jobs) as pool:
        futures = [pool.submit(apkexists, a) for a in array[front_num:]]
        kwargs = {
            'total': len(futures),
            'unit': 'it',
            'unit_scale': True,
            'leave': True
        }
        for f in tqdm(as_completed(futures), **kwargs):
            pass

    out = []
    for i, future in tqdm(enumerate(futures)):
        try:
            out.append(future.result())
        except Exception as e:
            out.append(e)
    results = front + out
    results = [i for i in results if i]
    return [i for sublist in results for i in sublist]

def replaceBuildVersion(file_path):
    newline = 'buildToolsVersion "%s"' % (BUILD_VERSION)
    fh, abs_path = mkstemp()
    with open(abs_path,'w') as new_file:
        if not os.path.isfile(file_path):
            return
        with open(file_path) as old_file:
            for line in old_file:
                result, count = re.subn(VERSION_PATTERN, newline, line)
                if count > 0:
                    logger.debug('Replaced buildversion with {}'.format(result))
                new_file.write(result)
    close(fh)
    remove(file_path)
    move(abs_path, file_path)

def replaceGradleClasspath(file_path):
    newline = 'classpath \'com.android.tools.build:gradle:{}\''.format(GRADLE_CLASSPATH)
    logger.debug('Replacing classpath in {}'.format(file_path))
    fh, abs_path = mkstemp()
    with open(abs_path,'w') as new_file:
        if not os.path.isfile(file_path):
            return
        with open(file_path) as old_file:
            for line in old_file:
                result,count = re.subn(PATTERN_GRADLE_CLASSPATH, newline, line.rstrip())
                if count > 0:
                    logger.debug('Replaced classpath with {}'.format(result))
                new_file.write(result + '\n')
    close(fh)
    remove(file_path)
    move(abs_path, file_path)

def replaceGradleUrl(file_path):
    newline = 'distributionUrl=http\://services.gradle.org/distributions/gradle-{}-bin.zip'.format(GRADLE_VERSION)
    logger.debug('Replacing url in {}'.format(file_path))
    fh, abs_path = mkstemp()
    with open(abs_path,'w') as new_file:
        if not os.path.isfile(file_path):
            return
        with open(file_path) as old_file:
            for line in old_file:
                result,count = re.subn(PATTERN_GRADLE_URL, newline, line.rstrip())
                if count > 0:
                    logger.debug('Replaced url with {}'.format(result))
                new_file.write(result + '\n')
    close(fh)
    remove(file_path)
    move(abs_path, file_path)

def stopGradleDaemons(rootdir):
    logger.debug('Stopping gradle daemons')
    cmd = '/home/yannes/Documents/development/gradle-4.4.1/bin/gradle --stop'
    task = Command(cmd, cwd = rootdir, printout = False)
    log = task.run()
    logger.debug('\n'.join(log))

def resetDirectory(args, rootdir, data):
    logger.info('Resetting directory: {}'.format(rootdir))
    cmd = 'git reset --hard HEAD'
    task = Command(cmd, cwd = rootdir, printout = args['verbose'])
    log = task.run()

def initDirectory(args, rootdir, data, count = 0):
    if count > 0:
        logger.info('Initializing directory (again): {}, dirtype = {}'.format(rootdir, args['dirtype']))
    else:
        logger.info('Initializing directory: {}, dirtype = {}'.format(rootdir, args['dirtype']))
    # cmd = 'source setup.sh; setupbuild'
    sdk = os.environ['ANDROID_SDK']

    if args['dirtype'] in ['gradle', 'ant', 'maven', 'android']: # == 'gradle':
        with open(rootdir + '/local.properties', 'w') as f:
            f.write('sdk.dir=' + sdk + '\n')
            if 'ANDROID_NDK' in os.environ:
                f.write('ndk.dir=' + os.environ['ANDROID_NDK'] + '\n')
        # copy('{}/Dropbox/Apps/StudioProjects/AndroidTemplate/build.gradle'.format(os.environ['HOME']), rootdir)
        # cmd = '/home/yannes/Documents/development/gradle-4.4.1/bin/gradle wrapper --gradle-version=4.4.1'
        cmd = 'gradle wrapper --gradle-version=4.4.1'
        logger.info('Initializing: {}'.format(cmd))
        task = Command(cmd, cwd = rootdir, printout = args['verbose'], error_pattern = ANT_ERROR_PATTERN, timeout = TASK_TIMEOUT)
        log = task.run()
        if os.path.exists('{}/build.gradle'.format(rootdir)):
            fix_gradle('{}/build.gradle'.format(rootdir))

        os.chmod('%s/gradlew' % (rootdir), 0755)
        logger.debug('Setup gradle: {}'.format(log))
        files = FindFilesRecursive(rootdir, 'gradle-wrapper.properties')
        for f in files:
            replaceGradleUrl(f)

        files = FindFilesRecursive(rootdir, 'build.gradle')
        logger.debug('Updating build version to {}'.format(BUILD_VERSION))
        for f in files:
            replaceBuildVersion(f)
            replaceGradleClasspath(f)
        if not log and count < 2:
            logger.debug('Error initializing:\n{}'.format(task.getLog()))
            initDirectory(args, rootdir, data, count = count + 1)
        elif not log and count >= 2:
            logger.info('Error initializing:\n{}'.format('\n'.join(task.getLog())))
        # replaceGradleUrl('{}/gradle/wrapper/gradle-wrapper.properties'.format(rootdir))
    elif args['dirtype'] in ['ant', 'maven', 'android']:
        activity = data['activity'].split('/')[-1].split('.')[-1].replace('-', '')
        appname = data['appname'].replace('-', '')
        if '.' in appname:
            appname = appname.split('.')[-1]
        if not activity.strip():
            activity = 'MainActivity'
        logger.info('{}: Converting {} directory to gradle'.format(appname, args['dirtype']))

        # androidcmd = '{}/tools_r25.2.5/android'.format(os.environ['ANDROID_SDK'])
        androidcmd = 'android'
        cmd = '{} create project --gradle --gradle-version 0.11.+ --path . --activity {} -t 2 -n {} -k {}'.format(androidcmd, activity, appname, data['package'])
        logger.info('Initializing: {}'.format(cmd))
        task = Command(cmd, cwd = rootdir, printout = args['verbose'])
        log = task.run()
        if os.path.exists('{}/build.gradle'.format(rootdir)):
            fix_gradle('{}/build.gradle'.format(rootdir))
        args['dirtype'] = 'gradle'
        logger.debug('Log: {}'.format(log))
    return args

def build(args, rootdir, data):
    logger.info('Building directory: {}'.format(rootdir))
    if args['dirtype'] == 'gradle':
        # cmd = './gradlew --project-cache-dir ~/Desktop/gradle-cache assembleDebug'
        cmd = 'gradle --project-cache-dir ~/Desktop/gradle-cache assembleDebug'
        err_pattern = [DEFAULT_ERROR_PATTERN, ANT_ERROR_PATTERN]
    elif args['dirtype'] == 'ant':
        cmd = 'ant debug'
        err_pattern = ANT_ERROR_PATTERN
    elif args['dirtype'] == 'maven':
        cmd = 'mvn build -DskipTests'
        err_pattern = ANT_ERROR_PATTERN
    logger.info('Building: ' + cmd)
    task = Command(cmd, cwd = rootdir, printout = args['verbose'], error_pattern = err_pattern, timeout = TASK_TIMEOUT)
    log = task.run()
    # return False if not log else True
    if not log:
        logger.error('Failed:\n{}'.format('\n'.join(task.getLog())))
        return False
    files = FindFilePatternRecursive(rootdir, '.apk')
    if not files:
        return False
    return True

def install(args, rootdir, data):
    apkpaths = apkexists(rootdir)
    if apkpaths:
        logger.info('Installing {} from adb'.format(apkpaths))
        cmd = 'adb install -r {}'.format(apkpaths[0])
    elif args['dirtype'] == 'gradle':
        cmd = './gradlew --project-cache-dir ~/Desktop/gradle-cache installDebug'
    elif args['dirtype'] == 'ant':
        cmd = 'ant install'
    elif args['dirtype'] == 'maven':
        cmd = 'mvn install -DskipTests'
    logger.info('Installing: ' + cmd)
    task = Command(cmd, cwd = rootdir, printout = args['verbose'])
    log = task.run()
    '''
    if not log:
        logger.error('Failed:\n{}'.format('\n'.join(task.getLog())))
        apkpaths = apkexists(rootdir)
        logger.info('Installing {} from adb'.format(apkpaths))
        if apkpaths:
            cmd = 'adb install {}'.format(apkpaths[0])
            task = Command(cmd, cwd = rootdir, printout = args['verbose'])
            log = task.run()
    '''

def test(args, rootdir, data):
    # cd to rootdir
    cmd = './gradlew --project-cache-dir ~/Desktop/gradle-cache cAT'
    logger.info('Testing: ' + cmd)
    task = Command(cmd, cwd = rootdir, printout = args['verbose'])
    log = task.run()

def launch(args, data):
    cmd = createLaunchCommand(data.get('package'), data.get('activity'), data.get('launcher'))
    logger.info('Launching: ' + cmd)
    task = Command(cmd, printout = args['verbose'])
    log = task.run()
    # print log

def clean(args, rootdir, data):
    # cd to rootdir
    if args['dirtype'] == 'gradle':
        cmd = './gradlew --project-cache-dir ~/Desktop/gradle-cache clean'
    elif args['dirtype'] == 'ant':
        cmd = 'ant clean'
    elif args['dirtype'] == 'maven':
        cmd = 'mvn clean'
    logger.info('Cleaning: ' + cmd)
    task = Command(cmd, cwd = rootdir, printout = args['verbose'])
    log = task.run()

def uninstall(args, rootdir, data):
    # cd to rootdir
    if args['dirtype'] == 'gradle':
        cmd = './gradlew --project-cache-dir ~/Desktop/gradle-cache uninstallAll'
    elif args['dirtype'] == 'ant':
        cmd = 'ant uninstall'
    elif args['dirtype'] == 'maven':
        cmd = 'mvn uninstall'
    logger.info('Uninstalling: ' + cmd)

    task = Command(cmd, cwd = rootdir, printout = args['verbose'])
    log = task.run()

def copyapk(args, rootdir, data):
    files = FindFilePatternRecursive(rootdir, '.apk')
    if not files:
        return False
    newname = '{}/{}.apk'.format(args['outdir'], data['appname'])

    if len(files) > 1:
        logger.info('Found multiple apk files: {}'.format(files))
    logger.info('Copying {} to {}'.format(files[0], newname))
    if args['dryrun']:
        return True
    copyfile(files[0], newname)
    return True

def checkError(log):
    for line in log:
        if line.strip().endswith('does not exist.'):
            return ERROR_NOTINSTALLED

if __name__ == '__main__':
    args = parseArgs()

    print args
    rootdir = args.get('rootdir')
    os.chdir(rootdir)
    rootdir = os.getcwd()
    filename = FindManifestFileRecursive(rootdir)
    if not filename:
        print 'Error: cannot find an AndroidManifest.xml in {}'.format(rootdir)
        sys.exit(1)
    # filename = args.get('manifestfile')[0]
    print 'Using manifest file: ', filename
    data = parseManifest(filename, activity = args.get('activity'))

    if args.get('dryrun'):
        sys.exit(0)

    if 'init' in args.get('stage') or 'clean' in args.get('stage'):
        initDirectory(args, rootdir, data)

    if 'build' in args.get('stage') and not 'install' in args.get('stage'):
        build(args, rootdir, data)

    if 'install' in args.get('stage'):
        install(args, rootdir, data)

    if 'launch' in args.get('stage'):
        launch(args, data)

    if 'clean' in args.get('stage'):
        clean(args, rootdir, data)
