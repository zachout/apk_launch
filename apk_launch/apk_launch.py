#!/usr/bin/env python
"""
    apk_launch performs various tasks for building Android projects

    Copyright (C) 2018 Zach Yannes
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>
"""
import os, sys
reload(sys)
sys.setdefaultencoding('utf-8')
from multiprocessing import Pool
import subprocess, threading
import signal
import time
import argparse
import re
sys.path.insert(0, '{}/Dropbox/Apps/python/apk_analyzer'.format(os.environ['HOME']))
from mycommand import Command
import xmltodict, json

from tempfile import mkstemp
from shutil import move, copy, copytree, copyfile
from os import remove, close
from antbuilder import *
from apk_utils import *
from tqdm import *
import logging
import logging.handlers
from concurrent.futures import ProcessPoolExecutor, as_completed
logging.basicConfig(level=logging.INFO, format = '%(message)s')
logger = logging.getLogger(__name__)

# LINE_PACKAGE = re.compile('package=[\"\'](?P<package>[^\"]+)[\"\']')
# LINE_ACTIVITY = re.compile('')

ERROR_NOTINSTALLED = 3
BUILD_VERSION = '27.0.3' # '26.0.1' # '25.0.3'

VERSION_PATTERN = re.compile(r'buildToolsVersion "(?P<version>[\w\d\.]+)"')
ANT_ERROR_PATTERN = re.compile('(?P<fail>BUILD FAILED)')

SOURCE_DIRECTORY = '/media/yannes/research/benchmark_sources/'
APK_DIRECTORY = '/media/yannes/research/benchmark_apks/'

from sys import platform
if not os.environ['JAVA_HOME']:
    if platform == 'darwin':
        os.environ['JAVA_HOME'] = '/Library/Java/JavaVirtualMachines/jdk1.8.0_144.jdk/Contents/Home'
    elif platform == 'linux' or platform == 'linux2':
        os.environ['JAVA_HOME'] = '/usr/lib/jvm/jdk1.8.0_121'
    logger.debug('Updated JAVA_HOME to {}'.format(os.environ['JAVA_HOME']))
else:
    logger.debug('Using JAVA_HOME to {}'.format(os.environ['JAVA_HOME']))


def parseArgs():
    parser = argparse.ArgumentParser(
        prog='apk_launch.py',
        description='Build and launch apk',
    )

    parser.add_argument('-s', '--stage',
                        help = 'Run stage(s)',
                        choices = ['init', 'build', 'install', 'launch',
                                   'clean', 'uninstall', 'test', 'copy', 'reset', 'all'],
                        # nargs = '+')
                        action = 'append')

    parser.add_argument('-d', '--dryrun',
                        help = 'Do not run the command(s)',
                        action = 'store_true',
                        default = False)

    parser.add_argument('-f', '--force',
                        help = 'Force rebuild of apk',
                        action = 'store_true')

    parser.add_argument('-a', '--activity',
                        help = 'Activity to launch')

    parser.add_argument('-n', '--appname', help = 'Application Name')

    parser.add_argument('-o', '--outdir',
                        help = 'Directory to store apks',
                        default = SOURCE_DIRECTORY)

    parser.add_argument('-v', '--verbose',
                        help = 'Prints command output to screen',
                        action = 'store_true',
                        default = False)

    parser.add_argument('--outfile',
                        help = 'Prints results to json file',
                        default = 'apk_results.json')
                        # default = '{}/apk_results.json'.format(SOURCE_DIRECTORY))

    '''
    parser.add_argument('rootdir',
                        help = 'Root directory of Android Manifest',
                        # action = 'append')
                        nargs = '+')
    '''
    parser.add_argument('apps',
                        help = 'Filename of app root directories or directory name')

    parser.add_argument('--list',
                        help = 'List all Applications',
                        action = 'store_true')

    parser.add_argument('-g', '--gradle',
                        help = 'Only list gradle files',
                        action = 'store_true')

    parser.add_argument('--listapks',
                        help = 'List all compiled debug apks',
                        action = 'store_true')

    parser.add_argument('--appsfile',
                        help = 'File to store applications directory list',
                        default = 'apps_list.txt')

    parser.add_argument('--logfile',
                        help = 'Log info to file',
                        default = 'apk_launch.log')
                        # default = '{}/apklaunch.log'.format(SOURCE_DIRECTORY))
    parser.add_argument('--count',
                        help = 'Max number of apps to build',
                        type = int)

    gradle_group = parser.add_mutually_exclusive_group()
    gradle_group.add_argument('--assume-gradle',
                              help = 'Assume all directories are gradle builds',
                              dest='assume_type',
                              action = 'store_true')
    gradle_group.add_argument('--dont-assume',
                              help = 'Do not assume directory build type',
                              dest='assume_type',
                              action = 'store_false')

    parser.set_defaults(assume_type = True)
    args, unknown = parser.parse_known_args()
    args = vars(args)

    global logger
    if args['verbose']:
        logging.basicConfig(level=logging.DEBUG, format = '%(message)s')
        logger = logging.getLogger(__name__)
        logger.setLevel(logging.DEBUG)
        logging.getLogger('apk_utils').setLevel(logging.DEBUG)
    else:
        logging.basicConfig(level=logging.INFO, format = '%(message)s')
        logger = logging.getLogger(__name__)
        logger.setLevel(logging.INFO)
        logging.getLogger('apk_utils').setLevel(logging.INFO)

        '''
        handler = logging.FileHandler(args['logfile'], mode = 'w')
        formatter = logging.Formatter('%(asctime)-15s %(levelname)-8s %(message)s')
        handler.setFormatter(formatter)

        logger = logging.getLogger()
        logger.setLevel(logging.INFO)
        logger.handlers = [handler]
        '''

    if not args['stage']:
        args['stage'] = ['init', 'build']
    if 'all' in args['stage']:
        args['stage'] = ['init', 'build', 'install', 'launch']
    logger.debug('Args: {}'.format(args))

    return args

def parseLine(pattern, line):
    m = re.match(pattern, line)
    if m:
        return m.groupdict()
    return None

def apklaunch(args, rootdir, pbar):
    rows, columns = os.popen('stty size', 'r').read().split()
    width = int(columns) - 60
    shortdir = '...' + rootdir[-width+3:] if len(rootdir) > width else rootdir
    pbar.set_description('Processing    {text:>{len}}'.format(len = width, text = shortdir))
    logger.debug('\n[apklaunch] Running on directory {}'.format(rootdir))
    dirtype = GetProjectType(rootdir)

    if args['assume_type']:
        dirtype = 'gradle'

    if not dirtype:
        return None
    args['dirtype'] = dirtype
    filename = findManifestFile(rootdir)

    os.chdir(rootdir)
    data = parseManifest(filename, activity = args.get('activity'))
    logger.debug('Data: {}'.format(data))
    if not data:
        logger.warning('parseManifest returned null for {}'.format(rootdir))
        return None

    if not args['appname']:
        # rootdir = args['rootdir']
        if rootdir.endswith('/'):
            appname = rootdir[:-1]
        else:
            appname = rootdir
        data['appname'] = appname.split('/')[-1]
    else:
        data['appname'] = args['appname']

    apkpaths = apkexists(rootdir)
    if apkpaths and not args['force']:
        logger.debug('Apk already exists: {}'.format(apkpaths))
        # newname = '{}/{}.apk'.format(args['outdir'], data['appname'])
        logger.info('Apk {} already exists. Skipping...'.format(data['appname']))
        data['paths'] = apkpaths
        return data

    try:
        if data and 'activity' in data and '/' not in data.get('activity', ''):
            activity = data.get('activity', '')
            if not activity.startswith('.'):
                activity = '.' + activity
            data['activity'] = '{}/{}'.format(data.get('package'), activity)
    except:
        logger.error('Data: {}'.format(data))


    if args.get('dryrun'):
        return None

    rootdir = os.getcwd()
    '''
    if 'build' in args['stage']:
        stopGradleDaemons(rootdir)
    '''

    for stage in args['stage']:
        if stage == 'init':
            pbar.set_description('Initializing  {text:>{len}}'.format(len = width, text = shortdir))
            args = initDirectory(args, rootdir, data)
        elif stage == 'build':
            pbar.set_description('Building      {text:>{len}}'.format(len = width, text = shortdir))
            res = build(args, rootdir, data)
            if not res:
                logger.info('BUILD FAILED')
                return None
            else:
                logger.info('BUILD SUCCEEDED')
        elif stage == 'install':
            pbar.set_description('Installing    {text:>{len}}'.format(len = width, text = shortdir))
            install(args, rootdir, data)
        elif stage == 'launch':
            launch(args, data)
        elif stage == 'test':
            test(args, rootdir, data)
        elif stage == 'uninstall':
            uninstall(args, rootdir, data)
        elif stage == 'copy':
            res = copyapk(args, rootdir, data)
            if not res:
                return None
        elif stage == 'clean':
            clean(args, rootdir, data)
        elif stage == 'reset':
            pbar.set_description('Resetting     {text:>{len}}'.format(len = width, text = shortdir))
            resetDirectory(args, rootdir, data)

    return data

def list_directories(topdir, prefix = None):
    return [os.path.join(topdir, name) for name in os.listdir(topdir) if os.path.isdir(os.path.join(topdir, name))]

def getAllDirectories(args):
    # srcdir = '/media/yannes/research/benchmark_sources/'
    if not os.path.isdir(args['apps']):
        raise ValueError('apps argument must be a directory when using --list')
    logger.info('Fetching application directories from {}'.format(args['apps']))
    dirs = []

    for d in tqdm(list_directories(args['apps']), desc = 'Fetching directories'):
        dirs += list_directories(d)

    logger.debug('App dirs: {}'.format(dirs))
    return dirs

def listApps(args, dirs):
    # gradle_dirs = sorted([d for d in tqdm(dirs) if IsGradleProject(d)])
    all_dirs = {}
    all_dirs['gradle'] = []
    all_dirs['nongradle'] = []
    all_dirs['nonandroid'] = []
    MAXLEN = 40
    maxlen = max([len(d) for d in dirs])
    maxlen = maxlen if maxlen < MAXLEN-3 else MAXLEN-3
    pbar = tqdm(dirs, desc = 'Processing projects')
    for d in pbar:
        text = '...' + d[-maxlen:]
        if len(text) == MAXLEN-3:
            text = '...' + text
        pbar.set_description('Processing {text:{len}}'.format(len = maxlen, text = text))
        subprojs = IsGradleProject(d)
        '''
        if subprojs:
            all_dirs['gradle'] += subprojs
        else:
            all_dirs['nongradle'].append(d)
        '''
        if not subprojs:
            if IsAndroidProject(d):
                all_dirs['nongradle'].append(d)
                continue
            else:
                logger.warning('Not an Android Project: {}'.format(d))
                all_dirs['nonandroid'].append(d)
        else:
            for path in subprojs:
                if IsAndroidProject(path):
                    all_dirs['gradle'].append(path)
                else:
                    logger.warning('Not an Android Project: {}'.format(path))
                    all_dirs['nonandroid'].append(path)

    # all_dirs = sorted(all_dirs)
    all_dirs['gradle'] = sorted(all_dirs['gradle'], key = lambda x: (x.count('/'), x))
    all_dirs['nongradle'] = sorted(all_dirs['nongradle'], key = lambda x: (x.count('/'), x))

    for key in all_dirs:
        for d in all_dirs[key]:
            logger.info('{} directory: {}'.format(key.title(), d))

    logger.info('{} Gradle Projects'.format(len(all_dirs['gradle'])))
    logger.info('{} Non-Gradle Projects'.format(len(all_dirs['nongradle'])))
    logger.info('{} Non-Android Projects'.format(len(all_dirs['nonandroid'])))

    file_prefix = '.'.join(args['appsfile'].split('.')[:-1])
    ext = args['appsfile'].split('.')[-1]
    for key in all_dirs:
        outfile = '{}_{}.{}'.format(file_prefix, key, ext)

        if os.path.exists(outfile):
            print 'File {} exists'.format(outfile)
            choice = ''
            while choice.lower() not in ['y', 'n']:
                choice = raw_input('Replace file? [y/n] ')
            if choice.lower() == 'n':
                logger.info('Not overwriting file {}'.format(outfile))
                sys.exit(0)

        with open(outfile, 'w') as f:
            f.write('\n'.join(all_dirs[key]))

    return all_dirs

def listApks(args, copyfiles = True):
    apks = parallel_apkexists(args['rootdir'])
    apks = [a for a in apks if a]
    logger.info('Found {} apks'.format(len(apks)))
    lines = []
    for apk in tqdm(apks):
        if not apk:
            continue
        prefix = apk.replace(SOURCE_DIRECTORY, '')
        logger.debug('APK {}: prefix {}'.format(apk, prefix))
        tokens = [t for t in prefix.split('/') if t]
        category = tokens[0]
        package = tokens[1]
        logger.debug('{cat}: {apk}'.format(cat = category, apk = apk))
        line = {'category': category, 'apk': apk}
        lines.append(line)
        outpath = '{}/{}'.format(APK_DIRECTORY, category)
        if copyfiles:
            if not os.path.exists(outpath):
                logger.info('Creating directory: {}'.format(outpath))
                os.makedirs(outpath)
            outfile = '{}/{}-debug.apk'.format(outpath, package)
            count = 1
            while os.path.exists(outfile):
                oldoutfile = outfile
                outfile = '{}/{}-debug-{}.apk'.format(outpath, package, count)
                # outfile = '{}-{}.apk'.format('.'.join(oldoutfile.split('.')[:-1]), count)
                count += 1
                logger.info('Apk {} exists. Trying {}...'.format(oldoutfile, outfile))
            logger.info('Copying {} to {}'.format(apk, outfile))
            copyfile(apk, outfile)

    logger.info('Writing log to {}'.format(args['outfile']))
    with open(args['outfile'], 'w') as f:
        for line in lines:
            text = json.dumps(line, sort_keys = True)
            f.write(text + '\n')

if __name__ == '__main__':
    args = parseArgs()
    rootdirs = []

    if args['list']:
        dirs = getAllDirectories(args)
        all_dirs = listApps(args, dirs)
        sys.exit(0)

    if os.path.isfile(args['apps']):
        handler = logging.FileHandler(args['logfile'], mode = 'w')
        formatter = logging.Formatter('%(asctime)-15s %(levelname)-8s %(message)s')
        handler.setFormatter(formatter)

        logger = logging.getLogger()
        logger.setLevel(logging.INFO)
        if args['verbose']:
            logger.setLevel(logging.DEBUG)
        logger.handlers = [handler]

        with open(args['apps'], 'r') as f:
            for line in f.readlines():
                if line.strip():
                    rootdirs.append(line.strip())
    elif os.path.isdir(args['apps']):
        rootdirs = [args['apps']]
    else:
        logger.error('Error: invalid input for apps: {}'.format(args['apps']))
        sys.exit(1)
    args['rootdir'] = rootdirs

    if args['count']:
        cnt = args['count']
        if cnt > len(args['rootdir']) - 1:
            cnt = len(args['rootdir']) - 1
        args['rootdir'] = args['rootdir'][:cnt]

    logger.debug('Root directories: {}'.format(args['rootdir']))

    apkjson = {}
    apkjson['success'] = []
    apkjson['failure'] = []
    pbar = tqdm(args['rootdir'])

    if args['listapks']:
        logger.info('Listing apks...')
        listApks(args, copyfiles =  not args['dryrun'])
        sys.exit(0)

    MAXLEN = 40
    maxlen = max([len(d) for d in args['rootdir']])
    maxlen = maxlen if maxlen < MAXLEN-3 else MAXLEN-3
    for rootdir in pbar:
        data = apklaunch(args, rootdir, pbar)
        if not data:
            apkjson['failure'].append(rootdir)
        else:
            newname = '{}/{}.apk'.format(args['outdir'], data['appname'])
            apkjson['success'].append(newname)

    if len(args['stage']) == 1 and 'reset' in args['stage']:
        logger.info('Reset all directories')
        sys.exit(0)

    if 'build' in args['stage'] and len(args['rootdir']) > 1:
        if args['outfile']:
            logger.info('Writing log to {}'.format(args['outfile']))
            with open(args['outfile'], 'w') as f:
                json.dump(apkjson, f, sort_keys = True, indent = 4)

            apks = apkjson['success']
            outfile = '{}/ready_apks.json'.format(SOURCE_DIRECTORY)
            with open(outfile, 'w') as f:
                json.dump(apks, f, sort_keys = True) # , indent = 4)
