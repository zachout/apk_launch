#!/usr/bin/env python
"""
    antbuilder - script to build old Android projects in ant format
    
    Copyright (C) 2018 Zach Yannes
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>
"""
import os, sys
import subprocess, threading
import signal
import time
import argparse
import re
from mycommand import Command
import xmltodict, json

from tempfile import mkstemp
from shutil import move
from os import remove, close

import logging
# logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)

handler = logging.StreamHandler()
handler.setLevel(logging.INFO)
logger.addHandler(handler)

ERROR_NOTINSTALLED = 3
BUILD_VERSION = '26.0.1' # '25.0.3'

VERSION_PATTERN = re.compile(r'buildToolsVersion "(?P<version>[\w\d\.]+)"')

def parseArgs():
    parser = argparse.ArgumentParser(
        prog='PROG',
        description='Build and launch apk using Apache Ant',
    )

    parser.add_argument('rootdir',
                        help = 'Root directory of Android Manifest')

    parser.add_argument('-s', '--stage',
                        help = 'Run stage(s)',
                        choices = ['init', 'build', 'install', 'launch', 'clean'],
                        default = ['init', 'build', 'install', 'launch'],
                        nargs = '+')

    parser.add_argument('-d', '--dryrun',
                        help = 'Do not run the app',
                        action = 'store_true',
                        default = False)

    parser.add_argument('-a', '--activity',
                        help = 'Activity to launch')

    parser.add_argument('-l', '--launch',
                        action = 'store_true',
                        default = False)

    args, unknown = parser.parse_known_args()

    args = vars(args)

    if args.get('launch'):
        args['stage'] = ['launch']

    return args

def FindFilesRecursive(rootdir, filename, parent = None):
    found = []
    logger.info('Recursively searching {} for {}'.format(rootdir, filename))
    for root, subdir, files in os.walk(rootdir):
        logger.debug('Root {}, Subdirectories: {}'.format(root, subdir))
        if parent and not root.endswith(parent):
            continue
        if filename in files:
            # return '{}/{}'.format(root, filename)
            logger.info('Found {} in {}'.format(filename, root, filename))
            found.append('{}/{}'.format(root, filename))
    return found

def FindManifestFileRecursive(rootdir):
    logger.info('Searching {} for AndroidManifest.xml'.format(rootdir))
    files = FindFilesRecursive(rootdir, 'AndroidManifest.xml')
    if not files:
        return None
    return files[0]

def IsGradleProject(rootdir):
    files = FindFilesRecursive(rootdir, 'build.gradle', parent = 'app')
    if not files:
        return False
    return True

def parseLine(pattern, line):
    m = re.match(pattern, line)
    if m:
        return m.groupdict()
    return None

def checkLauncherActivity(activity):
    if isinstance(activity.get('intent-filter'), dict):
        filters = [activity.get('intent-filter')]
    else:
        filters = activity.get('intent-filter')
    if not filters:
        return None
    for filter in filters:
        name = activity.get('@android:name')
        if isinstance(filter, dict):
            actions = [filter.get('action')]
        else:
            actions = filter.get('action')
        category = filter.get('category')
        for action in actions:
            if not action or not category:
                return None

            if isinstance(action, dict):
                actnames = [action.get('@android:name')]
            else:
                actnames = [act.get('@android:name') for act in action]

            if isinstance(category, dict):
                cats = [category.get('@android:name')]
            else:
                cats = [cat.get('@android:name') for cat in category]

            for actname in actnames:
                for cat in cats:
                    if actname == 'android.intent.action.MAIN' \
                            and cat == 'android.intent.category.LAUNCHER':
                        return name
    return None

def parseManifest(filename, activity = None):
    data = {}
    with open(filename, 'r') as f:
        lines = f.readlines()

    text = '\n'.join(lines)
    o = xmltodict.parse(text)

    xmldict = eval(json.dumps(o))
    manifest = xmldict.get('manifest')

    data['package'] = manifest.get('@package')
    app = manifest.get('application')
    acts = app.get('activity')
    data['launcher'] = False
    if isinstance(acts, dict):
        acts = [acts]

    for act in acts:
        name = act.get('@android:name')
        logger.debug('Checking act %s: "%s"' % (activity, name))

        if activity and activity in act.get('@android:name'):
            data['activity'] = act.get('@android:name')
            break

        if not activity:
            actName = checkLauncherActivity(act)
            if actName:
                data['activity'] = actName
                data['launcher'] = True
                break

    return data

def createLaunchCommand(package, activity, launcher = False):
    cmd = 'adb shell am start -n %s/%s' % (package, activity)
    if launcher:
        cmd = 'adb shell monkey -p %s -c android.intent.category.LAUNCHER 1' % (package)
    return cmd

def replaceBuildVersion(file_path):
    newline = 'buildToolsVersion "%s"' % (BUILD_VERSION)

    fh, abs_path = mkstemp()
    with open(abs_path,'w') as new_file:
        with open(file_path) as old_file:
            for line in old_file:
                result = re.sub(VERSION_PATTERN, newline, line)
                new_file.write(result)
    close(fh)
    remove(file_path)
    move(abs_path, file_path)

def initDirectory(rootdir, data):
    logger.info('Initializing directory: {}'.format(rootdir))
    # cmd = 'source setup.sh; setupbuild'
    sdk = os.environ['ANDROID_SDK']
    print 'Using sdk: ', sdk

    with open(rootdir + '/local.properties', 'w') as f:
        f.write('sdk.dir=' + sdk + '\n')

    name = rootdir.split('/')[-1]
    cmd = 'android update project --path . -t 2 -n {}'.format(name)
    task = Command(cmd, cwd = rootdir, printout = True)
    log = task.run()

def build(rootdir, data):
    # cd to rootdir
    cmd = 'ant debug'
    logger.info('Building: {}'.format(cmd))
    task = Command(cmd, cwd = rootdir, printout = True)
    log = task.run()

def install(rootdir, data):
    # cd to rootdir
    cmd = 'ant install'
    logger.info('Installing: {}'.format(cmd))
    task = Command(cmd, cwd = rootdir, printout = True)
    log = task.run()

def launch(data):
    cmd = createLaunchCommand(data.get('package'), data.get('activity'), data.get('launcher'))
    logger.info('Launching: {}'.format(cmd))
    task = Command(cmd, printout = True)
    log = task.run()
    # print log

def clean(rootdir, data):
    # cd to rootdir
    cmd = 'ant clean'
    logger.info('Cleaning: {}'.format(cmd))
    task = Command(cmd, cwd = rootdir, printout = True)
    log = task.run()

def checkError(log):
    for line in log:
        if line.strip().endswith('does not exist.'):
            return ERROR_NOTINSTALLED

if __name__ == '__main__':
    args = parseArgs()

    print args
    rootdir = args.get('rootdir')
    os.chdir(rootdir)
    rootdir = os.getcwd()
    filename = FindManifestFileRecursive(rootdir)
    if not filename:
        print 'Error: cannot find an AndroidManifest.xml in {}'.format(rootdir)
        sys.exit(1)
    # filename = args.get('manifestfile')[0]
    print 'Using manifest file: ', filename
    data = parseManifest(filename, activity = args.get('activity'))

    if args.get('dryrun'):
        sys.exit(0)

    if 'init' in args.get('stage') or 'clean' in args.get('stage'):
        initDirectory(rootdir, data)

    if 'build' in args.get('stage') and not 'install' in args.get('stage'):
        build(rootdir, data)

    if 'install' in args.get('stage'):
        install(rootdir, data)

    if 'launch' in args.get('stage'):
        launch(data)

    if 'clean' in args.get('stage'):
        clean(rootdir, data)
