#!/usr/bin/env python
"""
    copy_aosp_apks is a helper script for copying apks from AOSP 

    Copyright (C) 2018 Zach Yannes
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>
"""
import os, sys
reload(sys)
sys.setdefaultencoding('utf-8')
from multiprocessing import Pool
import subprocess, threading
import signal
import time
import argparse
import re
sys.path.insert(0, '{}/Dropbox/Apps/python/apk_analyzer'.format(os.environ['HOME']))
from mycommand import Command
import xmltodict, json

from tempfile import mkstemp
from shutil import move, copy, copytree, copyfile
from os import remove, close
from antbuilder import *
from apk_utils import *
from tqdm import *
import logging
import logging.handlers
from concurrent.futures import ProcessPoolExecutor, as_completed
logging.basicConfig(level=logging.INFO, format = '%(message)s')
logger = logging.getLogger(__name__)

# LINE_PACKAGE = re.compile('package=[\"\'](?P<package>[^\"]+)[\"\']')
# LINE_ACTIVITY = re.compile('')

ERROR_NOTINSTALLED = 3
BUILD_VERSION = '27.0.3' # '26.0.1' # '25.0.3'

VERSION_PATTERN = re.compile(r'buildToolsVersion "(?P<version>[\w\d\.]+)"')
ANT_ERROR_PATTERN = re.compile('(?P<fail>BUILD FAILED)')

AOSP_OUT = '/media/yannes/research/aosp_o/out/target/product/generic'
APK_DIRECTORY = '/media/yannes/research/aosp_apks'
AAPT_CMD = '/home/yannes/android-sdk/build-tools/27.0.3/aapt'
MAX_COPIES = 3

from sys import platform
if not os.environ['JAVA_HOME']:
    if platform == 'darwin':
        os.environ['JAVA_HOME'] = '/Library/Java/JavaVirtualMachines/jdk1.8.0_144.jdk/Contents/Home'
    elif platform == 'linux' or platform == 'linux2':
        os.environ['JAVA_HOME'] = '/usr/lib/jvm/jdk1.8.0_121'
    logger.debug('Updated JAVA_HOME to {}'.format(os.environ['JAVA_HOME']))
else:
    logger.debug('Using JAVA_HOME to {}'.format(os.environ['JAVA_HOME']))

APK_DIR = ['data/app', 'system', 'vendor']

def parseArgs():
    parser = argparse.ArgumentParser(
        prog=__file__,
        description='Copy (and rename) apks from aosp out directory',
    )

    parser.add_argument('-d', '--dryrun',
                        help = 'Do not run the command(s)',
                        action = 'store_true',
                        default = False)

    parser.add_argument('-o', '--outdir',
                        help = 'Directory to store apks',
                        default = APK_DIRECTORY)

    parser.add_argument('-v', '--verbose',
                        help = 'Prints command output to screen',
                        action = 'store_true',
                        default = False)

    parser.add_argument('--apkdirs',
                        help = 'Directory name',
                        default = ['{0}/{1}'.format(AOSP_OUT, d) for d in APK_DIR])

    args, unknown = parser.parse_known_args()
    args = vars(args)

    global logger
    if args['verbose']:
        logging.basicConfig(level=logging.DEBUG, format = '%(message)s')
        logger = logging.getLogger(__name__)
        logger.setLevel(logging.DEBUG)
        logging.getLogger('apk_utils').setLevel(logging.DEBUG)
    else:
        logging.basicConfig(level=logging.INFO, format = '%(message)s')
        logger = logging.getLogger(__name__)
        logger.setLevel(logging.INFO)
        logging.getLogger('apk_utils').setLevel(logging.INFO)

    logger.debug('Args: {}'.format(args))

    return args

def parseLine(pattern, line):
    m = re.match(pattern, line)
    if m:
        return m.groupdict()
    return None

def list_directories(topdir, prefix = None):
    return [os.path.join(topdir, name) for name in os.listdir(topdir) if os.path.isdir(os.path.join(topdir, name))]

def getAllDirectories(args):
    # srcdir = '/media/yannes/research/benchmark_sources/'
    if not os.path.isdir(args['apps']):
        raise ValueError('apps argument must be a directory when using --list')
    logger.info('Fetching application directories from {}'.format(args['apps']))
    dirs = []

    for d in tqdm(list_directories(args['apps']), desc = 'Fetching directories'):
        dirs += list_directories(d)

    logger.debug('App dirs: {}'.format(dirs))
    return dirs

PATTERN_PACKAGE = re.compile("""package: name='(?P<package>[^']+)'\s*versionCode='(?P<versioncode>\d+)'\s*versionName='(?P<versionName>[^']*)'\s*platformBuildVersionName='(?P<platformBuildVersionName>[^\']*)'""")
def getApplicationPackage(apkfile):
    cmd = '{0} dump badging {1}'.format(AAPT_CMD, apkfile)
    proc = Command(cmd)
    log = proc.run()
    logger.debug('Log: {0}'.format(log))
    for line in log:
        data = parseLine(PATTERN_PACKAGE, line)
        if data:
            return data.get('package')
    return None

def listApks(args, dirname, copyfiles = True):
    logger.debug('Fetching apks from {0}'.format(dirname))
    pattern = re.compile('.+\.apk')
    # apks = parallel_apkexists(dirname)
    # apks = apkexists(dirname)
    apks = FindFilePatternFast(dirname, pattern)
    apks = [a for a in apks if a]
    logger.info('Found {} apks'.format(len(apks)))
    lines = []
    for apk in tqdm(apks, leave = False):
        if not apk:
            continue
        package = getApplicationPackage(apk)
        logger.debug('APK {}: package {}'.format(apk, package))
        if not package:
            logger.error('Error: could not find package for {0}'.format(apk))
            continue
        # lines.append(apk)
        if copyfiles and args['outdir']:
            outfile = '{}/{}-debug.apk'.format(args['outdir'], package)
            count = 1
            while os.path.exists(outfile) and count < MAX_COPIES:
                oldoutfile = outfile
                outfile = '{}/{}-debug-{}.apk'.format(args['outdir'], package, count)
                # outfile = '{}-{}.apk'.format('.'.join(oldoutfile.split('.')[:-1]), count)
                count += 1
                logger.info('Apk {} exists. Trying {}...'.format(oldoutfile, outfile))
            logger.info('Copying {} to {}'.format(apk, outfile))
            copyfile(apk, outfile)

if __name__ == '__main__':
    args = parseArgs()
    for d in args['apkdirs']:
        listApks(args, d, copyfiles =  not args['dryrun'])
