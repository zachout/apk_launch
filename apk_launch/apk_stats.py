#!/usr/bin/env python
"""
    apk_stats calculates statistics about apks

    Copyright (C) 2018 Zach Yannes
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>
"""
import os, sys
import re
# from datetime import datetime
import argparse

from collections import OrderedDict
import gzip
import errno
import json
import happybase
import pprint
import logging
logging.basicConfig(level=logging.INFO, format = '%(message)s')
# logging.basicConfig()
logger = logging.getLogger(__name__)

APKANALYZER = '{}/tools/bin/apkanalyzer'.format(os.environ['ANDROID_SDK'])
from mycommand import Command

PACKAGE_PATTERN = re.compile('P\s*(?P<state>[xkrd])\s*(?P<defined>[\d]+)\s*(?P<referenced>[\d]+)\s*(?P<size>[\d]+)\s*(?P<name>[^\s]+)\s*(?P<extra>.*)')
CLASS_PATTERN = re.compile('C\s*(?P<state>[xkrd])\s*(?P<defined>[\d]+)\s*(?P<referenced>[\d]+)\s*(?P<size>[\d]+)\s*(?P<name>[^\s]+)')
# METHOD_PATTERN = re.compile('M\s*(?P<state>[xkrd])\s*(?P<defined>[\d]+)\s*(?P<referenced>[\d]+)\s*(?P<size>[\d]+)\s*(?P<name>[^\s]+)\s*(?P<method>.*)')
METHOD_PATTERN = re.compile('M\s*(?P<state>[xkrd])\s*(?P<defined>[\d]+)\s*(?P<referenced>[\d]+)\s*(?P<size>[\d]+)\s*(?P<name>[^\s]+)\s*(?P<methodname>[^\(]+)\((?P<params>[^)]*)\)')
FIELD_PATTERN = re.compile('F\s*(?P<state>[xkrd])\s*(?P<defined>[\d]+)\s*(?P<referenced>[\d]+)\s*(?P<size>[\d]+)\s*(?P<name>[^\s]+)\s*(?P<data_type>[^\s]+)\s*(?P<field>.*)')

import tree_utils
import apk_analyzer
import hbase_utils
import parallel_utils
import threading
from tqdm import tqdm

CATEGORIES = [
    'Connectivity', 'Development', 'Games', 'Graphics', 'Internet', 'Money',
    'Multimedia', 'Navigation', 'Phone_%26_SMS', 'Reading', 'Science_%26_Education',
    'Security', 'Sports_%26_Health', 'System', 'Theming', 'Time', 'Writing'
]

def valid_category(cat):
    try:
        idx = CATEGORIES.index(cat)
        return CATEGORIES[idx]
    except ValueError:
        msg = "Not a valid category: '{0}'.".format(cat)
        logger.error(msg)
        raise ValueError

def parseArgs():
    parser = argparse.ArgumentParser(
        prog='PROG',
        description='Analyze Android apk file',
    )

    parser.add_argument('input',
                        help = 'Input (apk or directory)')

    parser.add_argument('--category',
                        help = 'Application category',
                        type = valid_category)

    parser.add_argument('-o', '--outdir',
                        help = 'Directory to output apk data into',
                        default = '.')

    parser.add_argument('-d', '--dryrun',
                        help = 'Do not save trees to file',
                        action = 'store_true')

    parser.add_argument('-v', '--verbose',
                        help = 'Print debug information',
                        action = 'store_true')

    parser.add_argument('-s', '--store',
                        help = 'Store data to hbase',
                        action = 'store_true')

    parser.add_argument('--logfile',
                        help = 'Log debug info to file',
                        default = '{}/Dropbox/Apps/python/apk_analyzer/apk_stats.log'.format(os.environ['HOME']))

    args, unknown = parser.parse_known_args()
    args = vars(args)

    '''
    handler = logging.FileHandler(args['logfile'], mode = 'w')
    formatter = logging.Formatter('%(asctime)-15s %(levelname)-8s %(message)s')
    handler.setFormatter(formatter)
    logger = logging.getLogger()
    logger.handlers = [handler]
    '''

    if args['verbose']:
        logger.setLevel(logging.DEBUG)
        logging.getLogger('hbase_utils').setLevel(logging.DEBUG)
    else:
        logger.setLevel(logging.INFO)

    logger.info('Args: {}'.format(args))
    return args

def count_states(items):
    defined = apk_analyzer.match_state(items, 'd')
    referenced = apk_analyzer.match_state(items, 'r')

    return OrderedDict([
        ('defined', len(defined)),
        ('referenced', len(referenced)),
    ])

def printDict(data, depth = 0, singleline = False):
    if singleline:
        text = ', '.join(['{}: {}'.format(key, data[key]) for key in data])
        logger.info('{indent}{text}'.format(indent = ' ' * depth, text = text))
        return

    maxwidth = max([len(key) for key in data])
    for key in data:
        if isinstance(data[key], dict):
            logger.info('{indent}{text:>{width}}:'.format(
                        indent = ' ' * depth,
                        text = key, width = maxwidth))
            printDict(data[key], depth = depth + maxwidth + 1) # , singleline = True)
        else:
            logger.info('{indent}{text:>{width}}: {val}'.format(
                        indent = ' ' * depth,
                        text = key, width = maxwidth, val = data[key]))

def apk_stats(apkfile, category):
    cmd_ids = [
        ('summary', apk_analyzer.APK_SUMMARY),
        ('file_size', apk_analyzer.APK_FILESIZE),
        ('download_size', apk_analyzer.APK_DOWNLOADSIZE),
        ('min_sdk', apk_analyzer.APK_MINSDK),
        ('target_sdk', apk_analyzer.APK_TARGETSDK),
        ('permissions', apk_analyzer.APK_PERMISSIONS),
        ('packages', apk_analyzer.APK_DEXPACKAGES),
    ]
    cmds = []
    for cmd_str, cmd_id in cmd_ids:
        cmd = apk_analyzer.apk_command(cmd_id, {'apk': apkfile})
        cmd.start()
        cmds.append((cmd_str, cmd))

    items = OrderedDict()
    items['category'] = category
    for cmd_str, cmd in tqdm(cmds, leave = False):
        logger.debug('Joining {}'.format(cmd_str))
        cmd.join()
        log = cmd.getLog()
        if cmd_str == 'packages':
            # lines = cmd.wait().split('\n')
            classes, fields, methods, packages = apk_analyzer.parse_dex_packages(log.split('\n'))
            items['classes'] = classes # count_states(classes)
            items['fields'] = fields # count_states(fields)
            items['methods'] = methods # count_states(methods)
            # items['packages'] = count_states(packages)
        elif cmd_str == 'summary':
            tokens = log.split()
            items['package'] = tokens[0]
            items['version_code'] = tokens[1] # int(tokens[1])
            if len(tokens) >= 3:
                items['version_name'] = tokens[2]
        elif cmd_str in ['file_size', 'download_size', 'min_sdk', 'target_sdk']:
            items[cmd_str] = log.strip() # int(log)
        else:
            items[cmd_str] = [l.strip() for l in log.split('\n') if l] # log.replace('\n', '\n\t').rstrip()

    # logger.debug('Value list: {}'.format(text))
    apkname = apkfile.split('/')[-1]
    text = json.dumps(items, sort_keys = False)
    return json.loads(text)

def main(args):
    tokens = args['input'].split('/')
    category = valid_category(tokens[-2])
    apkname = tokens[-1]
    if args['input'].endswith('.apk'):
        data = apk_stats(args['input'], category)
        logger.debug(pprint.pformat(data, indent = 4))
        if args['store']:
            conn = happybase.Connection('localhost')
            hbase_utils.store_apk_data(conn, data)
    else:
        logger.error('Error: invalid input "{}"'.format(args['input']))

if __name__ == '__main__':
    args = parseArgs()
    if not args['input']:
        line = sys.stdin.readline()
        logger.info('Read in "{}"'.format(line))
        args['input'] = line.rstrip()
    main(args)
