#!/usr/bin/env python
"""
    mycommand is a helper library for running bash commands in python

    Copyright (C) 2018 Zach Yannes
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>
"""
import os, sys
import subprocess, threading
import re
import signal
import logging
# logging.basicConfig(level=logging.INFO)
logging.basicConfig(format = '%(message)s', level=logging.INFO)
logger = logging.getLogger(__name__)

class Alarm(Exception):
    pass

def alarm_handler(signum, frame):
    raise Alarm

class Command(object):
    def __init__(self, cmd, cwd = None, printout = False, error_pattern = None, queue = None, timeout = 0):
        self.cmd = cmd
        self.cwd = cwd
        self.printout = printout
        self.process = None
        self.log = []
        self.returncode = -1
        self.error_pattern = None
        if error_pattern:
            self.error_pattern = error_pattern if isinstance(error_pattern, list) else [error_pattern]
        self.queue = queue
        self.timeout = timeout

    def run(self, wait = True):
        if self.cwd:
            self.process = subprocess.Popen(self.cmd, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, bufsize=1,
                                            shell=True, universal_newlines = True, cwd = self.cwd)
        else:
            self.process = subprocess.Popen(self.cmd, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, bufsize=1,
                                            shell=True, universal_newlines = True)

        if wait:
            signal.signal(signal.SIGALRM, alarm_handler)
            signal.alarm(self.timeout)
            try:
                for line in iter(self.process.stdout.readline, ''):
                    line = line.strip()
                    if self.printout:
                        logger.info(line)
                    self.log.append(line)
                    if self.checkForErrors(line):
                        signal.alarm(0)
                        return None
                self.process.stdout.close()
                self.pid = self.process.pid
                pid = self.pid
                self.process.wait()
                signal.alarm(0)
            except Alarm:
                logger.error('{} timed out after {} seconds'.format(self.cmd, self.timeout))

        signal.alarm(0)
        return self.log

    def wait(self):
        # self.process.wait()
        outs, errs = self.process.communicate()
        # logger.debug('{}: output {}, stderr {}'.format(self.cmd, outs, errs))
        self.log = outs.rstrip()
        return self.log

    def checkForErrors(self, line):
        if not self.error_pattern:
            return False
        for pattern in self.error_pattern:
            if self.checkError(pattern, line):
                logger.error('Error running "{}" in {}'.format(self.cmd, self.cwd))
                self.process.kill()
                return True
        return False

    def checkError(self, pattern, line):
        m = re.match(pattern, line)
        if not m:
            return False
        data = m.groupdict()
        if not data:
            return False
        return True

    def getLog(self):
        return self.log
