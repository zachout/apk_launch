from distutils.core import setup
setup(
    name = 'apk_launch',
    packages = ['apk_launch'],
    version = '0.1',
    description = 'Tools for performing various tasks on Android projects',
    author = 'Zach Yannes',
    author_email = 'zachyannes@gmail.com',
    url = '',
    keywords = ['android', 'build', 'analyze', 'dex'],
    classifiers = [],
)
